package alma

import "testing"

func TestMoreResults(t *testing.T) {
	limit := 100
	offset := 0
	totalRecords := 150
	ap := NewAlmaPopulator()
	if !ap.moreResults(limit, offset, totalRecords) {
		t.Errorf("expected moreResults to return true")
	}

	limit = 100
	offset = 100
	totalRecords = 201
	ap = NewAlmaPopulator()
	if !ap.moreResults(limit, offset, totalRecords) {
		t.Errorf("expected moreResults to return true")
	}

}

func TestNoMoreResults(t *testing.T) {
	limit := 100
	offset := 99
	totalRecords := 100
	ap := NewAlmaPopulator()
	if ap.moreResults(limit, offset, totalRecords) {
		t.Errorf("expected moreResults to return false")
	}

	limit = 100
	offset = 300
	totalRecords = 299
	ap = NewAlmaPopulator()
	if ap.moreResults(limit, offset, totalRecords) {
		t.Errorf("expected moreResults to return false")
	}

	limit = 100
	offset = 100
	totalRecords = 101
	ap = NewAlmaPopulator()
	if ap.moreResults(limit, offset, totalRecords) {
		t.Errorf("expected moreResults to return false")
	}
}
