package alma

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/sirupsen/logrus"
	"gitlab.com/ucsdlibrary/development/alma-payment-etl/oracle"
	"go.elastic.co/ecslogrus"
)

func StubPaymentResponse(fixture string) (oracle.Payment, error) {
	jsonFile, err := ioutil.ReadFile(fixture)
	stubToken := &oracle.Token{
		AccessToken: "token",
	}

	if err != nil {
		fmt.Println(err)
	}

	var rawJson interface{}
	err = json.Unmarshal(jsonFile, &rawJson)
	if err != nil {
		panic(err.Error())
	}

	testServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(rawJson)
	}))

	defer testServer.Close()

	log := logrus.New()
	log.SetFormatter(&ecslogrus.Formatter{})

	ofcApi := oracle.OFCApiQuery{
		Endpoint: testServer.URL,
		Logger:   log,
	}

	return ofcApi.InvoiceQuery(stubToken, "1234-LIB")
}

func TestUpdatePaymentResponse(t *testing.T) {
	t.Setenv("ALMA_API_KEY", "alma-api-key")
	got, _ := StubSuccessfulHttpResponse("fixtures/test.json", http.StatusOK)

	if got == nil {
		t.Errorf("Expected an invoice returned from GetInvoices")
	}
	want := []Invoice{
		{
			Number: "12039123",
		},
	}

	if got[0].Number != want[0].Number {
		t.Errorf("Got Number %s did not match Wanted Number %s", got[0].Number, want[0].Number)
	}

	got_p, _ := StubPaymentResponse("fixtures/test-update-payment-response.json")

	want_p := oracle.Payment{
		InvoiceNumber: "12039123",
	}

	if got_p.InvoiceNumber != want_p.InvoiceNumber {
		t.Errorf("Got Number %s did not match Wanted Number %s", got_p.InvoiceNumber, want_p.InvoiceNumber)
	}

	testServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		w.Header().Set("Content-Type", "application/json")
	}))

	ap := NewAlmaPopulator()
	ap.BaseUrl = testServer.URL

	status, err := ap.UpdatePayment(got_p, got[0])

	if err != nil || status != 200 {
		t.Errorf("ERROR: StatusCode %d - %s", status, err)
	}
}

func TestInvalidUpdatePaymentResponse(t *testing.T) {
	t.Setenv("ALMA_API_KEY", "alma-api-key")
	got, _ := StubSuccessfulHttpResponse("fixtures/test.json", http.StatusOK)

	if got == nil {
		t.Errorf("Expected an invoice returned from GetInvoices")
	}

	got_p, _ := StubPaymentResponse("fixtures/test-no-payment-response.json")

	testServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		w.Header().Set("Content-Type", "application/json")
	}))

	ap := NewAlmaPopulator()
	ap.BaseUrl = testServer.URL

	_, err := ap.UpdatePayment(got_p, got[0])

	if !strings.Contains(err.Error(), "ERROR: No Payment Info found for Invoice") {
		t.Errorf("Expected error response to no payment invoice %s", err.Error())
	}
}
