package alma

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"

	"github.com/go-playground/validator/v10"
	"github.com/sirupsen/logrus"
	"gitlab.com/ucsdlibrary/development/alma-payment-etl/oracle"
)

const (
	defaultAPIURL       = "https://api-na.hosted.exlibrisgroup.com"
	defaultInvoiceQuery = "/almaws/v1/acq/invoices/"
)

var validate *validator.Validate

func invoicesURL(apiURL string, query string) string {
	return fmt.Sprintf("%s%s", apiURL, query)
}

func NewAlmaPopulator() *AlmaPopulator {
	validate = validator.New()

	return &AlmaPopulator{BaseUrl: defaultAPIURL, InvoicesQuery: defaultInvoiceQuery}
}

type AlmaPopulator struct {
	BaseUrl               string
	InvoicesQuery         string
	InvoiceWorkflowStatus string
	ApiKey                string
	Logger                *logrus.Logger
}

type Response struct {
	Invoices         []Invoice `json:"invoice" validate:"required,dive"`
	TotalRecordCount int       `json:"total_record_count" validate:"required"`
}

func (i *AlmaPopulator) getPaginatedInvoices(invoices []Invoice, limit, offset int) ([]Invoice, error) {
	query := fmt.Sprintf("%s?invoice_workflow_status=%s&apikey=%s&view=brief&limit=%d&offset=%d&format=json", i.InvoicesQuery, i.InvoiceWorkflowStatus, i.ApiKey, limit, offset)
	i.Logger.Info(fmt.Sprintf("Number of new Invoice records prior to query getPaginatedInvoices %d", len(invoices)))
	i.Logger.Info(fmt.Sprintf("Executing Alma getPaginatedInvoices query %s", query))

	response, err := http.Get(invoicesURL(i.BaseUrl, query))
	if err != nil {
		return nil, fmt.Errorf("error querying alma api %s: %s", query, err.Error())
	}

	data, err := io.ReadAll(response.Body)
	if err != nil {
		return nil, fmt.Errorf("error reading alma invoices query response body: %s", err.Error())
	}

	if response.StatusCode == http.StatusBadRequest {
		var errorResponse AlmaErrorResponse
		err = json.Unmarshal(data, &errorResponse)
		if err != nil {
			return nil, fmt.Errorf("error unmarshalling alma 400 error response body: %s", err.Error())
		}
		err = validate.Struct(errorResponse)
		if err != nil {
			return nil, err
		}
		return nil, fmt.Errorf("%+v", errorResponse)
	}

	var r Response
	err = json.Unmarshal(data, &r)
	if err != nil {
		return nil, fmt.Errorf("error unmarshalling alma invoices response body: %s", err.Error())
	}

	err = validate.Struct(r)
	if err != nil {
		return nil, err
	}

	invoices = append(invoices, r.Invoices...)

	i.Logger.Info(fmt.Sprintf("Number of new Invoice records after query %d", len(invoices)))
	if i.moreResults(limit, offset, r.TotalRecordCount) {
		i.Logger.Info(fmt.Sprintf("There are more new Invoices to retrieve. Have: %d Total: %d", len(invoices), r.TotalRecordCount))
		return i.getPaginatedInvoices(invoices, limit, limit+offset)
	} else {
		i.Logger.Info(fmt.Sprintf("Retrieved all new Invoices. Have: %d Total: %d", len(invoices), r.TotalRecordCount))
		return invoices, nil
	}
}

// Determine if another call to getPaginatedInvoices should be invoked
func (i *AlmaPopulator) moreResults(limit int, offset int, totalRecords int) bool {
	remaining := (totalRecords - offset) - limit
	if remaining > 0 {
		return true
	} else {
		return false
	}
}

func (i *AlmaPopulator) GetInvoices(limit_param ...int) ([]Invoice, error) {
	almaApiKey, ok := os.LookupEnv("ALMA_API_KEY")
	if !ok {
		return nil, fmt.Errorf("required environment variable ALMA_API_KEY not found")
	}

	i.ApiKey = almaApiKey

	var invoices []Invoice

	limit := 100
	if len(limit_param) > 0 {
		limit = limit_param[0]
	}
	var offset = 0
	return i.getPaginatedInvoices(invoices, limit, offset)
}

func (i *AlmaPopulator) UpdatePayment(p oracle.Payment, invoice Invoice) (int, error) {
	if p.CheckNumber == 0 {
		return 0, fmt.Errorf("ERROR: No Payment Info found for Invoice %s-LIB with id %s", invoice.Number, invoice.Id)
	}

	invoice.Payment.VoucherNumber = fmt.Sprint(p.CheckNumber)
	//invoice.Payment.VoucherDate = p.PaymentDate + "Z"
	invoice.Payment.VoucherDate = p.PaymentDate
	invoice.Payment.VoucherAmount = fmt.Sprint(invoice.TotalAmount)
	invoice.Payment.PaymentStatus.Value = "PAID"
	invoice.Payment.PaymentStatus.Desc = "Paid"
	invoice.InvoiceStatus.Value = "CLOSED"
	invoice.InvoiceStatus.Desc = "Closed"

	payload, _ := json.Marshal(invoice)
	fmt.Printf("Invoice Data will be sent %s\n", payload)

	query := fmt.Sprintf("%s%s?op=paid", i.InvoicesQuery, invoice.Id)

	almaApiKey, ok := os.LookupEnv("ALMA_API_KEY")

	if !ok {
		return 0, fmt.Errorf("required environment variable ALMA_API_KEY not found")
	}

	r, err := http.NewRequest("POST", invoicesURL(i.BaseUrl, query), bytes.NewBufferString(string(payload)))

	if err != nil {
		return 0, fmt.Errorf("%s", err.Error())
	}

	//set headers to the request
	r.Header.Add("Content-Type", "application/json")
	r.Header.Add("Authorization", fmt.Sprintf("apikey %s", almaApiKey))
	r.Header.Add("Accept", "application/json")

	if err != nil {
		return 0, err
	}

	//send request and get the response
	client := &http.Client{}
	res, err := client.Do(r)
	if err != nil {
		return 0, err
	}

	defer res.Body.Close()

	data, err := io.ReadAll(res.Body)
	if err != nil {
		return 0, fmt.Errorf("error reading Alma Invoices response: %s", err.Error())
	}

	if res.StatusCode == http.StatusBadRequest {
		var errorResponse AlmaErrorResponse
		err = json.Unmarshal(data, &errorResponse)
		if err != nil {
			return res.StatusCode, fmt.Errorf("error unmarshalling alma 400 error response body: %s", err.Error())
		}
		err = validate.Struct(errorResponse)
		if err != nil {
			return res.StatusCode, err
		}
		return res.StatusCode, fmt.Errorf("%+v", errorResponse)
	}

	return res.StatusCode, nil
}
