package alma

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"strings"
	"testing"

	"github.com/sirupsen/logrus"
	"go.elastic.co/ecslogrus"
)

func StubSuccessfulHttpResponse(fixture string, statusCode int) ([]Invoice, error) {
	jsonFile, err := os.ReadFile(fixture)
	if err != nil {
		fmt.Println(err)
	}

	var rawJson interface{}
	err = json.Unmarshal(jsonFile, &rawJson)
	if err != nil {
		panic(err.Error())
	}

	testServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(statusCode)
		json.NewEncoder(w).Encode(rawJson)
	}))

	defer testServer.Close()

	log := logrus.New()
	log.SetFormatter(&ecslogrus.Formatter{})

	ap := NewAlmaPopulator()
	ap.Logger = log
	ap.BaseUrl = testServer.URL
	ap.InvoiceWorkflowStatus = url.QueryEscape("Ready to be Paid")

	// set limit to 1 to test pagination
	return ap.GetInvoices(1)
}

func TestMissingAlmaApiKey(t *testing.T) {
	testServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte("Won't get this far"))
	}))

	defer testServer.Close()

	log := logrus.New()
	log.SetFormatter(&ecslogrus.Formatter{})

	ap := NewAlmaPopulator()
	ap.BaseUrl = testServer.URL
	ap.Logger = log
	ap.InvoiceWorkflowStatus = url.QueryEscape("Ready to be Paid")

	_, err := ap.GetInvoices()

	if err == nil {
		t.Errorf("Expected error missing ALMA_API_KEY")
	}

	if !strings.Contains(err.Error(), "ALMA_API_KEY") {
		t.Errorf("Expected error to contain reference to ALMA_API_KEY %s", err.Error())
	}
}

func TestErrorResponse(t *testing.T) {
	t.Setenv("ALMA_API_KEY", "alma-api-key")
	_, err := StubSuccessfulHttpResponse("fixtures/error-response.json", http.StatusBadRequest)

	if err == nil {
		t.Errorf("Expected error message from Alma request %d", http.StatusBadRequest)
	}
	if !strings.Contains(err.Error(), "ErrorCode:40166410 ErrorMessage:The parameter op is invalid. Received: invalid. Valid options are: [process_invoice, mark_in_erp, paid, rejected]") {
		t.Errorf("Expected error response to include ErrorCode and ErrorMessage %s", err.Error())
	}
}

func TestInvalidErrorResponse(t *testing.T) {
	t.Setenv("ALMA_API_KEY", "alma-api-key")
	_, err := StubSuccessfulHttpResponse("fixtures/invalid-error-response.json", http.StatusBadRequest)

	if err == nil {
		t.Errorf("Expected schema validation error from Alma response %d", http.StatusBadRequest)
	}

	if !strings.Contains(err.Error(), "Field validation for 'ErrorsExist' failed on the 'required' tag") {
		t.Errorf("Expected error response to include missing errorsExist property %s", err.Error())
	}
	if !strings.Contains(err.Error(), "Field validation for 'TrackingId' failed on the 'required' tag") {
		t.Errorf("Expected error response to include missing trackingId property %s", err.Error())
	}
}

func TestGetInvoicesResponse(t *testing.T) {
	t.Setenv("ALMA_API_KEY", "alma-api-key")
	got, _ := StubSuccessfulHttpResponse("fixtures/test.json", http.StatusOK)

	if got == nil {
		t.Errorf("Expected an invoice returned from GetInvoices")
	}
	want := []Invoice{
		{
			Number: "12039123",
		},
	}

	// Since total_record_count in `fixtures/test.json` is 5, we expect 5 Invoices to be returned from recursively calling getPaginatedInvoices when the `limit` is set to 1
	if len(got) != 5 {
		t.Errorf("Expected 5 invoices but got %d", len(got))
	}

	if got[0].Number != want[0].Number {
		t.Errorf("Got Number %s did not match Wanted Number %s", got[0].Number, want[0].Number)
	}

	// if !reflect.DeepEqual(got, want) {
	// 	t.Errorf("Got %v and Wanted %v", got, want)
	// }
}

func TestMissingRequiredFields(t *testing.T) {
	t.Setenv("ALMA_API_KEY", "alma-api-key")
	_, err := StubSuccessfulHttpResponse("fixtures/test-missing-required.json", http.StatusOK)

	if err == nil {
		t.Errorf("Expected a validation errors from GetInvoices")
	}
	if !strings.Contains(err.Error(), "'Number' failed on the 'required' tag") {
		t.Errorf("Expected error to mention missing the Number field %s", err.Error())
	}
	if !strings.Contains(err.Error(), "'TotalAmount' failed on the 'required' tag") {
		t.Errorf("Expected error to mention missing the TotalAmount field %s", err.Error())
	}
}
