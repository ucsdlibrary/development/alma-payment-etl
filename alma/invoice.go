package alma

// see: https://developers.exlibrisgroup.com/alma/apis/docs/xsd/rest_invoice.xsd/
type Invoice struct {
	Id             string `json:"id" validate:"required"`
	Number         string `json:"number" validate:"required"`
	InvoiceDate    string `json:"invoice_date" validate:"required"`
	InvoiceDueDate string `json:"invoice_due_date,omitempty"`
	Vendor         struct {
		Desc  string `json:"desc,omitempty"`
		Value string `json:"value"`
	} `json:"vendor,omitempty"`
	VendorAccount string  `json:"vendor_account,omitempty"`
	TotalAmount   float64 `json:"total_amount" validate:"required"`
	Currency      struct {
		Desc  string `json:"desc,omitempty"`
		Value string `json:"value"`
	} `json:"currency,omitempty"`
	TotalInvoiceLinesAmount float64 `json:"total_invoice_lines_amount,omitempty"`
	VendorContactPerson     struct {
		FirstName string `json:"first_name,omitempty"`
		LastName  string `json:"last_name,omitempty"`
	} `json:"vendor_contact_person,omitempty"`
	PaymentMethod struct {
		Desc  string `json:"desc,omitempty"`
		Value string `json:"value"`
	} `json:"payment_method,omitempty"`
	ReferenceNumber string `json:"reference_number,omitempty"`
	CreationForm    struct {
		Desc  string `json:"desc,omitempty"`
		Value string `json:"value"`
	} `json:"creation_form,omitempty"`
	Owner struct {
		Desc  string `json:"desc,omitempty"`
		Value string `json:"value"`
	} `json:"owner,omitempty"`
	InvoiceStatus struct {
		Desc  string `json:"desc,omitempty"`
		Value string `json:"value"`
	} `json:"invoice_status,omitempty"`
	InvoiceWorkflowStatus struct {
		Desc  string `json:"desc,omitempty"`
		Value string `json:"value"`
	} `json:"invoice_workflow_status,omitempty"`
	InvoiceApprovalStatus struct {
		Desc  string `json:"desc,omitempty"`
		Value string `json:"value"`
	} `json:"invoice_approval_status,omitempty"`
	ApprovedBy        string `json:"approved_by,omitempty"`
	ApprovalDate      string `json:"approval_date,omitempty"`
	AdditionalCharges struct {
		UseProRata bool    `json:"use_pro_rata"`
		Shipment   float64 `json:"shipment,omitempty"`
		Overhead   float64 `json:"overhead,omitempty"`
		Insurance  float64 `json:"insurance,omitempty"`
		Discount   float64 `json:"discount,omitempty"`
	} `json:"additional_charges,omitempty"`
	InvoiceVat struct {
		ReportTax         bool `json:"report_tax"`
		VatPerInvoiceLine bool `json:"vat_per_invoice_line"`
		VatCode           struct {
			Desc  string `json:"desc,omitempty"`
			Value string `json:"value"`
		} `json:"vat_code"`
		Percentage float64 `json:"percentage"`
		VatAmount  float64 `json:"vat_amount"`
		Type       struct {
			Desc  string `json:"desc,omitempty"`
			Value string `json:"value"`
		} `json:"type"`
		ExpendedFromFund bool   `json:"expended_from_fund"`
		VendorTax        string `json:"vendor_tax"`
	} `json:"invoice_vat,omitempty"`
	ExplicitRatio []ExplicitRatio `json:"explicit_ratio,omitempty"`
	Payment       struct {
		Prepaid       bool `json:"prepaid"`
		InternalCopy  bool `json:"internal_copy"`
		ExportToErp   bool `json:"export_to_erp"`
		PaymentStatus struct {
			Desc  string `json:"desc,omitempty"`
			Value string `json:"value"`
		} `json:"payment_status,omitempty"`
		VoucherDate     string `json:"voucher_date"`
		VoucherNumber   string `json:"voucher_number"`
		VoucherAmount   string `json:"voucher_amount"`
		VoucherCurrency struct {
			Desc  string `json:"desc,omitempty"`
			Value string `json:"value"`
		} `json:"voucher_currency"`
	} `json:"payment"`
	Alerts        []Alert `json:"alert,omitempty"`
	Notes         []Note  `json:"notes,omitempty"`
	NumberOfLines struct {
		Desc  string `json:"desc,omitempty"`
		Value int    `json:"value"`
	} `json:"number_of_lines,omitempty"`
}

type Alert struct {
	Desc  string `json:"desc,omitempty"`
	Value string `json:"value"`
}

type Note struct {
	NoteText string `json:"note_text"`
}

type ExplicitRatio struct {
	ForeignCurrency struct {
		Value string `json:"value"`
	} `json:"foreign_currency"`
	Rate int `json:"rate"`
}
