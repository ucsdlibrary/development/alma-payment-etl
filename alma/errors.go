package alma

// Errors from alma (in json) look like:
// {"errorsExist":true,"errorList":{"error":[{"errorCode":"40166410","errorMessage":"The parameter op is invalid. Received: invalid. Valid options are: [process_invoice, mark_in_erp, paid, rejected].","trackingId":"E01-0501011436-0YKOG-AWAE1024241656"}]},"result":null}

type AlmaErrorResponse struct {
	ErrorsExist bool   `json:"errorsExist" validate:"required"`
	Result      string `json:"result"`
	ErrorList   struct {
		Error []AlmaError `json:"error" validate:"required,dive"`
	} `json:"errorList" validate:"required"`
}

type AlmaError struct {
	ErrorCode    string `json:"errorCode" validate:"required"`
	ErrorMessage string `json:"errorMessage" validate:"required"`
	TrackingId   string `json:"trackingId" validate:"required"`
}
