# Alma Payment ETL

This application is responsible for querying updated payment information from the UCSD campus Oracle OFC API for Invoice
records that are managed in Alma.

It interacts with 2 API's to accomplish this:
- Oracle OFC
- Alma

### Local Development
To run the application locally, some environment variables must be set when the application is executed.

The application will load a `.env` file, if present, using the [godotenv][godotenv] package.

Use the provided `.env.sample` file:

```sh
$ cp env.sample .env
```

Then set the values of the required variables in `.env` (see below)

#### Required Variables

| Variable | Description |
| --------- | ----------- |
| `ALMA_API_KEY` | Found in [config][config] (staging and prod) |
| `EMAIL_FROM` | Default found in [values.yaml][values.yaml] |
| `EMAIL_TO` | Default found in [values.yaml][values.yaml] |
| `OFC_API_URL` | Found in [config][config] (staging and prod) |
| `OFC_CLIENT_SECRET` | Found in [config][config] (staging and prod) |
| `OFC_TOKEN_URL` | Found in [config][config] (staging and prod) |
| `SMTP_ADDRESS` | Default found in [values.yaml][values.yaml] |
| `SMTP_PASSWORD` | Found in [config][config] (staging and prod) |
| `SMTP_PORT` | Default found in [values.yaml][values.yaml] |
| `SMTP_USER_NAME` | Default found in [values.yaml][values.yaml] |


#### Running the Application
Once you have the `test.sh` script setup, you can run it.

You can simply do `./test.sh`

#### Debugging

##### VSCode
VSCode allows you to debug Go programs using the `delve` debugger in its Go extension. The following steps listed will assume that you have the Go extension installed and fully updated.

1) To get started, click on the `Run and Debug` tab of VSCode and click `create a launch.json file`.
2) We want to add `Go: Launch Package`
3) Next, to check if it's working, go to the `main.go` and press F5 to run your program and debug. A debugging toolbar should show up, as well as the `Run and Debug` tab, and the debug console.
4) To set a breakpoint, click to the left of a line number in the file or press F9 on the line you want to set a breakpoint. A small red dot should appear, and in the `Run and Debug` tab, a breakpoint should be added at the bottom of the tab.
5) Congrats, you have the basics of VSCode's `Run and Debug`, more in-depth applications of debugging can be found at the link below.

> See: https://github.com/golang/vscode-go/wiki/debugging

### API Documentation

#### Update Invoice(s)
`PUT` request made to update `Invoice` record in Alma:

https://developers.exlibrisgroup.com/alma/apis/docs/xsd/rest_invoice.xsd/?tags=PUT#invoice


#### List Invoice(s) needing Payment information
`GET` request made for listing `Invoice` records in Alma that we need the application to check against the Oracle
`/payments` API for updates

https://developers.exlibrisgroup.com/alma/apis/docs/acq/R0VUIC9hbG1hd3MvdjEvYWNxL2ludm9pY2VzLw==/

#### See Also

Original Jira ticket: https://ucsdlibrary.atlassian.net/browse/TDX-1729

[config]:https://gitlab.com/ucsdlibrary/devops/config/-/tree/trunk/project/alma-payment-etl
[godotenv]:https://github.com/joho/godotenv
[values.yaml]:chart/values.yaml
