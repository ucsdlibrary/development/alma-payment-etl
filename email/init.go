package email

import (
	"bytes"
	"fmt"
	"log"
	"net/smtp"
	"os"
	"strings"
)

type EmailNotification struct {
	Hostname      string
	Port          string
	Password      string
	Username      string
	Sender        string
	Recipient     string
	MessageBuffer bytes.Buffer
}

func NewEmailNotification() (*EmailNotification, error) {
	hostname, ok := os.LookupEnv("SMTP_ADDRESS")
	if !ok {
		return nil, fmt.Errorf("required environment variable SMTP_ADDRESS not found")
	}

	port, ok := os.LookupEnv("SMTP_PORT")
	if !ok {
		return nil, fmt.Errorf("required environment variable SMTP_PORT not found")
	}

	password, ok := os.LookupEnv("SMTP_PASSWORD")
	if !ok {
		return nil, fmt.Errorf("required environment variable SMTP_PASSWORD not found")
	}

	username, ok := os.LookupEnv("SMTP_USER_NAME")
	if !ok {
		return nil, fmt.Errorf("required environment variable SMTP_USER_NAME not found")
	}

	sender, ok := os.LookupEnv("EMAIL_FROM")
	if !ok {
		return nil, fmt.Errorf("required environment variable EMAIL_FROM not found")
	}

	recipient, ok := os.LookupEnv("EMAIL_TO")
	if !ok {
		return nil, fmt.Errorf("required environment variable EMAIL_TO not found")
	}
	return &EmailNotification{
		Hostname:  hostname,
		Port:      port,
		Password:  password,
		Username:  username,
		Sender:    sender,
		Recipient: recipient,
	}, nil
}

func (e *EmailNotification) Send() error {
	auth := smtp.PlainAuth("", e.Username, e.Password, e.Hostname)
	subject := "AP Batch Payment Update Status"
	server := fmt.Sprintf("%s:%s", e.Hostname, e.Port)
	msg := []byte("From: " + e.Sender + "\r\n" +
		"To: " + e.Recipient + "\r\n" +
		"Subject: " + subject + "\r\n\r\n" +
		e.MessageBuffer.String() + "\r\n")

	// multiple email recipients will be separated by a comma
	recipients := strings.Split(e.Recipient, ",")

	err := smtp.SendMail(server, auth, e.Sender, recipients, msg)

	if err != nil {
		return err
	}
	log.Println("Email sent successfully")
	return nil
}
