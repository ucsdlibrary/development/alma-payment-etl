package oracle

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

func StubSecrets(t *testing.T) {
	t.Setenv("ALMA_API_KEY", "apikey")
	t.Setenv("OFC_CLIENT_SECRET", "client-secret")
	t.Setenv("OFC_TOKEN_URL", "https://example.url/token")
	t.Setenv("ALMA_API_KEY", "apikey")
}

func TestGetToken(t *testing.T) {
	StubSecrets(t)
	apiClientSecret := os.Getenv("OFC_CLIENT_SECRET")

	testServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("grant_type", "client_credentials")
		w.Header().Add("Content-Type", "application/x-www-form-urlencoded")
		w.Header().Add("Authorization", fmt.Sprintf("Basic %s", apiClientSecret))
		r.URL.Path = "/token"
		if r.URL.Path != "/token" {
			t.Errorf("Expected to request '/token', got %s", r.URL.Path)
		}
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"access_token":"123"}`))
	}))

	defer testServer.Close()

	tokenPop := TokenPopulator{
		TokenEndpoint: testServer.URL,
	}
	got, _ := tokenPop.GetToken()

	var resp Token
	resp.AccessToken = "123"
	want := resp

	if got.AccessToken != want.AccessToken {
		t.Errorf("Got %v and Wanted %v", got, want)
	}
}

func TestNilTokenResponse(t *testing.T) {
	StubSecrets(t)
	apiClientSecret := os.Getenv("OFC_CLIENT_SECRET")

	testServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("grant_type", "client_credentials")
		w.Header().Add("Content-Type", "application/x-www-form-urlencoded")
		w.Header().Add("Authorization", fmt.Sprintf("Basic %s", apiClientSecret))
		r.URL.Path = "/token"
		if r.URL.Path != "/token" {
			t.Errorf("Expected to request '/token', got %s", r.URL.Path)
		}
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"token":"123"}`))
	}))

	defer testServer.Close()

	tokenPop := TokenPopulator{
		TokenEndpoint: testServer.URL,
	}
	got, _ := tokenPop.GetToken()

	var resp Token
	resp.AccessToken = ""
	want := resp

	if got != want {
		t.Errorf("Got %v and Want %v", got, want)
	}
}
