package oracle

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"reflect"
	"strings"
	"testing"

	"github.com/sirupsen/logrus"
	"go.elastic.co/ecslogrus"
)

func TestMissingOfcApiUrl(t *testing.T) {
	_, err := OfcApiQueryUrl()

	if err == nil {
		t.Errorf("Expected error missing OFC_API_URL %s", err.Error())
	}

	if !strings.Contains(err.Error(), "OFC_API_URL") {
		t.Errorf("Expected error to contain reference to OFC_API_URL %s", err.Error())
	}

}

// Confirm that a change in upstream API response triggers an appropriate error
// In this case, we're changing response attribute types
func TestChangedOfcApiResponse(t *testing.T) {
	stubToken := &Token{
		AccessToken: "token",
	}

	jsonFile, err := ioutil.ReadFile("fixtures/test-invalid-response.json")
	if err != nil {
		fmt.Println(err)
	}

	var rawJson interface{}
	err = json.Unmarshal(jsonFile, &rawJson)
	if err != nil {
		panic(err.Error())
	}

	testServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(rawJson)
	}))

	defer testServer.Close()

	ofcApi := OFCApiQuery{
		Endpoint: testServer.URL,
	}

	_, err = ofcApi.InvoiceQuery(stubToken, "1234-LIB")
	if err == nil {
		t.Errorf("Expected error unmarshalling to Payment struct %s", err.Error())
	}
}

// Confirm that we only receive a single Payment from the API in a response
func TestExtraPaymentsInApiResponse(t *testing.T) {
	stubToken := &Token{
		AccessToken: "token",
	}

	jsonFile, err := ioutil.ReadFile("fixtures/test-extra-payments.json")
	if err != nil {
		fmt.Println(err)
	}

	var rawJson interface{}
	err = json.Unmarshal(jsonFile, &rawJson)
	if err != nil {
		panic(err.Error())
	}

	testServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(rawJson)
	}))

	defer testServer.Close()

	ofcApi := OFCApiQuery{
		Endpoint: testServer.URL,
	}

	_, err = ofcApi.InvoiceQuery(stubToken, "1234-LIB")

	if !strings.Contains(err.Error(), "expected a single Payment response") {
		t.Errorf("Expected error as response contained multiple payments %s", err.Error())
	}
}

func TestInvoiceQueryResponse(t *testing.T) {
	stubToken := &Token{
		AccessToken: "token",
	}

	jsonFile, err := ioutil.ReadFile("fixtures/test-valid-response.json")
	if err != nil {
		fmt.Println(err)
	}

	var rawJson interface{}
	err = json.Unmarshal(jsonFile, &rawJson)
	if err != nil {
		panic(err.Error())
	}

	testServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(rawJson)
	}))

	defer testServer.Close()

	log := logrus.New()
	log.SetFormatter(&ecslogrus.Formatter{})

	ofcApi := OFCApiQuery{
		Endpoint: testServer.URL,
		Logger:   log,
	}

	got, _ := ofcApi.InvoiceQuery(stubToken, "1234-LIB")

	want := Payment{
		CheckNumber:   403616,
		InvoiceDate:   "2021-10-18",
		InvoiceId:     2333746,
		InvoiceNumber: "77696",
		PaymentDate:   "2021-12-21",
		PaymentId:     1675431,
		PaymentNumber: 403616,
	}

	if !reflect.DeepEqual(got, want) {
		t.Errorf("Got %v and Wanted %v", got, want)
	}
}
