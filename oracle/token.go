package oracle

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strings"
)

type Token struct {
	AccessToken string `json:"access_token"`
}

type TokenPopulator struct {
	TokenEndpoint string
}

// Get a token for making OFC API requests
func (tok *TokenPopulator) GetToken() (Token, error) {
	var t Token
	apiClientSecret, ok := os.LookupEnv("OFC_CLIENT_SECRET")
	if !ok {
		return t, fmt.Errorf("required environment variable OFC_CLIENT_SECRET not found")
	}

	data := url.Values{}
	data.Set("grant_type", "client_credentials")
	r, err := http.NewRequest("POST", tok.TokenEndpoint, strings.NewReader(data.Encode()))
	if err != nil {
		return t, err
	}

	//set headers to the request
	r.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	r.Header.Add("Authorization", fmt.Sprintf("Basic %s", apiClientSecret))

	//send request and get the response
	client := &http.Client{}
	res, err := client.Do(r)
	if err != nil {
		return t, fmt.Errorf("error aquiring token from OFC_TOKEN_URL. reason %s", err.Error())
	}
	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		return t, readErr
	}
	err = json.Unmarshal(body, &t)
	if err != nil {
		return t, fmt.Errorf("error finding token: %s", err.Error())
	}

	return t, nil
}
