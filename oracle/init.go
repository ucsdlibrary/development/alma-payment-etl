package oracle

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"

	"github.com/go-playground/validator/v10"
	"github.com/sirupsen/logrus"
)

var validate *validator.Validate

// Payment Details mapping OFC API GET response
type Payment struct {
	CheckNumber   int    `json:"CHECK_NUMBER" validate:"required"`
	InvoiceDate   string `json:"INVOICE_DATE" validate:"required"`
	InvoiceId     int    `json:"INVOICE_ID" validate:"required"`
	InvoiceNumber string `json:"INVOICE_NUMBER" validate:"required"`
	PaymentDate   string `json:"PAYMENT_DATE" validate:"required"`
	PaymentId     int    `json:"PAYMENT_ID" validate:"required"`
	PaymentNumber int    `json:"PAYMENT_NUMBER" validate:"required"`
}

type OFCApiQuery struct {
	Endpoint string
	Logger   *logrus.Logger
}

func OfcApiQueryUrl() (string, error) {
	apiUrl, ok := os.LookupEnv("OFC_API_URL")
	if !ok {
		return "", fmt.Errorf("required environment variable OFC_API_URL not found")
	}

	return apiUrl, nil
}

// Query to retrieve all invoices that do not yet have payment information in Alma
func (ofcApi *OFCApiQuery) InvoiceQuery(apiToken *Token, invoiceId string) (Payment, error) {
	var invoicePayment Payment
	r, err := http.NewRequest("GET", ofcApi.Endpoint, nil)
	r.URL.RawQuery = url.Values{
		"InvoiceNumber": {invoiceId},
	}.Encode()
	if err != nil {
		return invoicePayment, fmt.Errorf("error setting up OFC /invoice query: %s", ofcApi.Endpoint)
	}

	//set headers to the request
	r.Header.Add("Accept", "application/json")
	r.Header.Add("Authorization", fmt.Sprintf("Bearer %s", apiToken.AccessToken))

	//send request and get the response
	client := &http.Client{}
	res, err := client.Do(r)
	if err != nil {
		return invoicePayment, fmt.Errorf("error execturing OFC /invoice query: %s %s", ofcApi.Endpoint, err.Error())
	}

	resBody, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return invoicePayment, fmt.Errorf("error reading OFC /invoice query response body: %s", err.Error())
	}

	var payments []Payment
	err = json.Unmarshal(resBody, &payments)
	if err != nil {
		return invoicePayment, fmt.Errorf("error unmarshalling ofc payments response body: %s", err.Error())
	}

	validate = validator.New()

	if len(payments) != 1 {
		return invoicePayment, fmt.Errorf("expected a single Payment response. Instead got: %v+", payments)
	}

	// There is only one Payment in a successful response, since we are supplying a single InvoiceNumber
	invoicePayment = payments[0]
	err = validate.Struct(invoicePayment)
	if err != nil {
		return invoicePayment, fmt.Errorf("invoice %s does not have updated payment info in Oracle, skipping", invoicePayment.InvoiceNumber)
	}
	ofcApi.Logger.Info(fmt.Sprintf("Successfully received Payment with CHECK_NUMBER %d and INVOICE_ID %d from Oracle OFI API", invoicePayment.CheckNumber, invoicePayment.InvoiceId))
	return invoicePayment, nil
}
