ARG IMAGE_REGISTRY='docker.io/'
FROM ${IMAGE_REGISTRY}golang:1.21.11-alpine3.19 AS builder
ENV CGO_ENABLED 0
ENV GOOS linux
ENV GOARCH amd64
# Install git.
# Git is required for fetching the dependencies.
RUN apk update && apk add --no-cache git
WORKDIR $GOPATH/src/gitlab.com/ucsdlibrary/development/alma-payment-etl
COPY . .
# Fetch dependencies.
# Using go get.
RUN go get -d -v
# Build the binary.
RUN go build -ldflags="-w -s" -o /go/bin/alma-payment-etl

FROM ${IMAGE_REGISTRY}alpine:3.19 as certs
RUN apk --update add ca-certificates

FROM scratch as production
COPY --from=certs /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY --from=builder /go/bin/alma-payment-etl /go/bin/alma-payment-etl
# Run the alma-payment-etl binary.
ENTRYPOINT ["/go/bin/alma-payment-etl"]
