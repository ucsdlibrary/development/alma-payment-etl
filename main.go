package main

import (
	"encoding/json"
	"fmt"
	"net/url"
	"os"

	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
	"gitlab.com/ucsdlibrary/development/alma-payment-etl/alma"
	"gitlab.com/ucsdlibrary/development/alma-payment-etl/email"
	"gitlab.com/ucsdlibrary/development/alma-payment-etl/oracle"
	"go.elastic.co/ecslogrus"
)

func main() {
	log := logrus.New()
	log.SetFormatter(&ecslogrus.Formatter{})

	log.Info("Starting alma-payment-etl process")

	err := godotenv.Load()
	if err != nil {
		log.Warn("error loading .env file")
	}

	tokQueryUrl, ok := os.LookupEnv("OFC_TOKEN_URL")
	if !ok {
		log.Fatal("Required environment variable OFC_TOKEN_URL not found")
	}
	tokPopulator := oracle.TokenPopulator{
		TokenEndpoint: tokQueryUrl,
	}

	ofcQueryUrl, err := oracle.OfcApiQueryUrl()
	if err != nil {
		log.WithError(err).Fatal("Failed to setup OfcApiQueryUrl")
	}
	ofcApi := oracle.OFCApiQuery{
		Endpoint: ofcQueryUrl,
		Logger:   log,
	}

	log.Info("Requesting API Token for Oracle OFC API")
	apiToken, err := tokPopulator.GetToken()
	if err != nil {
		log.WithError(err).Fatal("Failed to receive an API token for Oracle OFC API")
	}
	log.Info(fmt.Sprintf("Successfully received API Token for Oracle OFC API %s", apiToken))

	invoices := alma.NewAlmaPopulator()
	invoices.Logger = log
	invoices.InvoiceWorkflowStatus = url.QueryEscape("Ready to be Paid")

	log.Println("Requesting Invoices in Alma without payment information")
	invoicesReq, err := invoices.GetInvoices()
	if err != nil {
		log.WithError(err).Fatal("Failed to get Invoices from Alma without payment information")
	}
	log.Info(fmt.Sprintf("Successfully received %d Invoices in Alma without payment information", len(invoicesReq)))

	email, emailErr := email.NewEmailNotification()
	if emailErr != nil {
		log.WithError(emailErr).Fatal("failed to setup email notification instance")
	}

	for _, invoice := range invoicesReq {
		log.Info(fmt.Sprintf("Querying for Invoice %s-LIB %s from Oracle OFC API\n", invoice.Number, invoice.Id))
		formattedInvoiceNumber := fmt.Sprintf("%s-LIB", invoice.Number)
		paymentData, err := ofcApi.InvoiceQuery(&apiToken, formattedInvoiceNumber)

		if err != nil {
			log.WithError(err).Warn(fmt.Sprintf("Oracle OFC API query failed for Invoice %s-LIB with id %s.", invoice.Number, invoice.Id))
			email.MessageBuffer.WriteString(err.Error() + "\n\n")
			continue
		}
		pData, _ := json.Marshal(paymentData)
		log.Info(fmt.Printf("Payment Data before update %s\n", pData))

		statusCode, err := invoices.UpdatePayment(paymentData, invoice)

		if err != nil || statusCode != 200 {
			log.WithError(err).Warn(fmt.Sprintf("Update Payment for Invoice %s-LIB with id %s failed. Status code %d", invoice.Number, invoice.Id, statusCode))
			email.MessageBuffer.WriteString("ERROR: Update Payment for Invoice %s-LIB with id %s failed. " + err.Error() + "\n\n")
			continue
		} else {
			email.MessageBuffer.WriteString("Successfully updated payment information for Invoice " + invoice.Number + " " + invoice.Id + "\n\n")
		}
	}

	err = email.Send()
	if err != nil {
		log.WithError(err).Fatal("error sending email notification")
	}
	log.Info("Finished alma-payment-etl process...")
}
