module gitlab.com/ucsdlibrary/development/alma-payment-etl

go 1.19

require (
	github.com/go-playground/validator/v10 v10.25.0
	github.com/joho/godotenv v1.5.1
	github.com/sirupsen/logrus v1.9.3
	go.elastic.co/ecslogrus v1.0.0
)

require (
	github.com/gabriel-vasile/mimetype v1.4.8 // indirect
	github.com/go-playground/locales v0.14.1 // indirect
	github.com/go-playground/universal-translator v0.18.1 // indirect
	github.com/leodido/go-urn v1.4.0 // indirect
	github.com/magefile/mage v1.9.0 // indirect
	golang.org/x/crypto v0.32.0 // indirect
	golang.org/x/net v0.34.0 // indirect
	golang.org/x/sys v0.29.0 // indirect
	golang.org/x/text v0.21.0 // indirect
)
